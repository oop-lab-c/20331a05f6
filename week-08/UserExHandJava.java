//Program to demonstrate User Defined Exception Handling in Java

import java.util.*;

 class MyException extends Exception{
   MyException(int n){
    System.out.println("Natural Number should be greater than zero");
  }
}

class UserExHandJava
{
  public static void main(String[] args){
   System.out.println("Enter Natural Number: ");
   Scanner input = new Scanner(System.in);
   int num = input.nextInt();
    if(num > 0){
      System.out.println(num+ " is a Natural Number");
    }else{
       try{
        throw new MyException(num);
      }catch(Exception e){
       System.out.println(e);
      }finally{
        System.out.println("All Exceptions are Handled..");
      }
    }
  }
}



