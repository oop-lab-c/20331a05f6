#include<iostream>
using namespace std;
class animal
{
    public:
    virtual void bark()=0;
};
class dog : public animal
{
    public:
    void bark()
    {
        cout<<" the dog barks as bow...bow..."<<endl;
    }
};
int main()
{
    dog d;
    d.bark();
    return 0;
}