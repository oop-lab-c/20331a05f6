#include<iostream>
using namespace std;
 
class parent{
    public:
    void schoolchoice(){                         
        cout<<"Sri Chaitanya Techno school"<<endl;
    }
    virtual void collegechoice()=0;               
};
class son : public parent{
    public:
    void collegechoice(){
        cout<<"Sri Chaitanya Junior College"<<endl;
    }
};
class daughter : public parent{
    public:
    void collegechoice(){
        cout<<"MVGR College of Engineering"<<endl;
    } 
};
int main(){
    son s;
    daughter d;
    cout<<"Son's Name of School and College"<<endl;
    s.schoolchoice();               
    s.collegechoice();             
    cout<<"Daughter's Name of School and College"<<endl;
    d.schoolchoice();               
    d.collegechoice();              
}
