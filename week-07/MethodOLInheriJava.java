//Program to demonstrate method overloading in java using inheritance

class parent {
    void add(String a , String b){         
        System.out.println(a+b);
    }
}
class child extends parent{
    void add(int a , int b){                  
        System.out.println(a+b);
    }
}
class MethodOLInheriJava{
    public static void main(String [] args){
        child obj = new child();
        obj.add("Water","melon");           
        obj.add(6,7);                       
    }
}

