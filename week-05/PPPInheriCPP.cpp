//Program to demonstrate public , protected , private Inheritances

#include<iostream>
using namespace std;

class base{
    public :
        int a=10;
    protected :
        float b=25.8;
    private :
        string c="sangeeta";
};
class derived1 : public base{
    //a is public //b is protected //c is not accessible  -- Public Inheritance
    public :
    void print(){
        cout<<"protected value "<<b<<endl;
    }
};
class derived2 : protected base{
    //a is protected //b is protected //c is not accessible  -- Protected Inheritance
    public :
    void print(){
        cout<<"public value "<<a<<endl;
        cout<<"protected value "<<b<<endl;
    }
};
class derived3 : private base{
    //a is private //b is private //c is not accessible  -- Private Inheritance 
    public :
    void print(){
        cout<<"public value "<<a<<endl;
        cout<<"protected value "<<b<<endl;
    }
};
int main(){
    derived1 d1;
    derived2 d2;
    derived3 d3;
    cout<<"public value "<<d1.a<<endl;
    d1.print();
    d2.print();
    d3.print();
}