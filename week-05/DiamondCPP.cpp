//Program to show diamond problem

#include<iostream>
using namespace std;
class base{     //Base class
    public:
    base(){
        cout<<"Base constructor called"<<endl;
    }
};
class father :  public base{    //Simple inheritance
    public:
    father(){
        cout<<"Father constructor called"<<endl;
    }
};
class mother : public base{     //Hierarchial inheritance
    public:
    mother(){
        cout<<"Mother constructor called"<<endl;
    }
};
class child : public father, public mother{     //Multiple inheritance
    public:
    child(){
        cout<<"Child constructor called"<<endl;
    }

};
int main(){
    cout<<"Diamond problem demonstration "<<endl;
    child obj;
    return 0;
}