//Program to demonstrate Simple Inheritance

#include<iostream>
using namespace std;

class common{       
    public:
    void subjects(){
        cout<<"PHYSICS, CHEMISTRY, ENGLISH, SANSKRIT "<<endl;
    }
};
class MPC : public common{      
    public:
    void extra(){
        cout<<"MATHS1A, MATHS1B "<<endl;
    }
};

int main(){
    MPC Obj;
    cout<<"MPC"<<endl;
    Obj.subjects();         
    Obj.extra();
}