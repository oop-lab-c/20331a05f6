//14 week 4
class ParamConstJava
{
    String fullName;
    double semPercentage;
    String collegeName;
    int collegeCode;
    public ParamConstJava()
    {
         collegeName = "MVGR";
         collegeCode = 33;
         System.out.println("College name is "+collegeName);
         System.out.println("college code is "+collegeCode);
    }
    public ParamConstJava(String x, double y)
    {
        fullName=x;
        semPercentage=y;
    }
    void display()
    {
        System.out.println("name is "+fullName);
        System.out.println("semister percentage is "+semPercentage);
    }
    public static void main(String[] args)  
    {
        ParamConstJava obj = new ParamConstJava();
        ParamConstJava obj1 = new ParamConstJava("david",57.679);
        obj1.display();
    }
}
